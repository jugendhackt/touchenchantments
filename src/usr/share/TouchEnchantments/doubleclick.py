#!/usr/bin/env python3

import time

from rightclick import IsNotTooFarAway

class DoubleClick:
    def __init__(self):
        self.last_time = time.time()
        self.last_pos_x = -100
        self.last_pos_y = -100

    def CheckForDoubleClick(self, _x, _y, _radius, _timeout):
        if time.time() - self.last_time <= _timeout and IsNotTooFarAway(self.last_pos_x, self.last_pos_y, _x, _y, _radius):
            self.last_time = time.time()
            self.last_pos_x = -100
            self.last_pos_y = -100
            return True
        else:
            self.last_pos_x = _x
            self.last_pos_y = _y
            self.last_time = time.time()
            return False
